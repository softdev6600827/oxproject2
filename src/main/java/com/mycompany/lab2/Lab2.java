/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab2 {

    static Scanner sc = new Scanner(System.in);
    static char player1 = 'X';
    static char player2 = 'O';
    static char currentPlayer;
    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static int row = 0;
    static int col = 0;

    public static void main(String[] args) {
        printWelcome();
        printChooseCurrentPlayer();
        inputCurrentPlayer();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();
            if (isWinner() == true) {
                printTable();
                System.out.println("Congratulations! Player " + currentPlayer + " wins!");
                break;
            } else if (isDraw() == true) {
                printTable();
                System.out.println("The result is Draw!");
                break;
            }
            switchPlayer();
        }
        inputContinue();
    }

    private static void printWelcome() {
        System.out.println("|--------------------|");
        System.out.println("| Welcome To OX Game |");
        System.out.println("|--------------------|");
    }

    private static void printChooseCurrentPlayer() {
        System.out.println("|---------------------|");
        System.out.println("| Choose who go first |");
        System.out.println("|    Press 1 is X     |");
        System.out.println("|    Press 2 is O     |");
        System.out.println("|---------------------|");

    }

    private static void inputCurrentPlayer() {
        System.out.print("Choose here : ");

        while (true) {
            currentPlayer = sc.next().charAt(0);
            if (currentPlayer == '1') {
                currentPlayer = player1;
                System.out.println("X go first");
                break;
            } else if (currentPlayer == '2') {
                currentPlayer = player2;
                System.out.println("O go first");
                break;
            } else {
                System.out.print("Choose again : ");
            }
        }

    }

    private static void printTable() {
        System.out.println("|-----------|");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println();
            System.out.println("|-----------|");
        }

    }

    private static void inputRowCol() {
        while (true) {
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;
            if (row >= 0 && row < 3 && col >= 0 && col < 3 && table[row][col] == '-') {
                table[row][col] = currentPlayer;
                break;
            } else {
                System.out.print("Invalid move. Please try again : ");
                sc.nextLine();
            }
        }
    }

    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWinner() {
        return checkRow() || checkCol() || checkDiagonal1() || checkDiagonal2();
    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return !isWinner();
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == currentPlayer && table[i][1] == currentPlayer && table[i][2] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[0][i] == currentPlayer && table[1][i] == currentPlayer && table[2][i] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkDiagonal1() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkDiagonal2() {
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static void printTurn() {
        System.out.print("It's player " + currentPlayer + " turn. Please input row[1-3] and column[1-3] for your move [ex:1 1]: ");
    }

    private static void inputContinue() {
        while (true) {            
            System.out.print("Do you want to play again? (y/n) : ");
            char ans = sc.next().charAt(0);
            if(ans == 'y'){
                resetGame();
                main(null);
                break;
            } else if(ans == 'n'){
                System.out.println("Thank you for playing! Goodbye!");
                break;
            }else{
                System.out.println("Invalid input. Please enter y or n ");
            }
        }
    }

    private static void resetGame() {
        table = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        currentPlayer = '\0';
    }

}
